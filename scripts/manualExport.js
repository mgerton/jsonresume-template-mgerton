// resume-cli has no way of exporting with a local theme yet, export it manually
// export method originally by [Joshua Button](https://github.com/jbttn)
// @see https://github.com/jbttn/jbttn.github.io/blob/master/resume/export.js

'use strict';

const fs = require('fs');
const render = require('../index').render;

let json = JSON.parse(fs.readFileSync(process.cwd() + '/resume.json', 'utf-8'));

fs.writeFile(process.cwd() + '/dist/index.html', render(json), function (err) {
  if (err) throw err;
});
