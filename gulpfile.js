var del = require('del');
var gulp = require('gulp');
var rsync = require('gulp-rsync');
var shell = require('gulp-shell');
var vinylPaths = require('vinyl-paths');
var moment = require('moment');

var config = {
	rsync: {
		destination: '/home/mgerton/resume.mattgerton.com',
		hostname: 'mattgerton.com',
		username: 'mgerton',
		root: 'dist',
		incremental: true,
		progress: true,
		relative: true,
		emptyDirectories: true,
		recursive: true,
		clean: true,
		exclude: ['.git', 'node_modules'],
		include: []
	}
};

// The generated filename for the PDF resume
function pdfFilename() {
	var username = 'mgerton';
	var prefix = ['resume_', username, '_'].join('');
	var dateTs = moment().format('YYYY-MM-DD');

	return [prefix, dateTs].join('');
}

/**
 * Callback function used to execute the rsync commands. This method forces Gulp
 * execute the callback after the build tasks has completed, as opposed to in
 * parallel if they were just defined inside of the array.
 */
function serverSync() {
	return gulp.src('dist')
		.pipe(rsync(config.rsync));
}

gulp.task('clean:dist', function () {
	return gulp.src('dist/*')
		.pipe(gulp.dest('dist'))
		.pipe(vinylPaths(del));
});

// Build tasks
gulp.task('build:custom:html', ['clean:dist'], shell.task('node scripts/manualExport.js'));

gulp.task('build:classy:html', ['clean:dist'], shell.task('resume export dist/index -t classy --format html'));
gulp.task('build:classy:pdf', ['clean:dist'], shell.task('resume export dist/' + pdfFilename() + ' -t classy --format pdf'));

// Deploy tasks
gulp.task('deploy:classy', ['build:classy:html'], serverSync);
gulp.task('deploy:custom', ['build:custom:html'], serverSync);

gulp.task('default', ['build:custom:html', 'deploy:custom']);
