'use strict';

const fs = require('fs');
const Handlebars = require('handlebars');
const schema = require('resume-schema');

const handlebarsHelpers = require('./src/helpers');

let json = schema.resumeJson;

function render(json) {
  const dateMap = {
    '01': 'January',
    '02': 'February',
    '03': 'March',
    '04': 'April',
    '05': 'May',
    '06': 'June',
    '07': 'July',
    '08': 'August',
    '09': 'September',
    '10': 'October',
    '11': 'November',
    '12': 'December',
  };

  // FIXME: this function mutates the source object passed in. needs refactoring.
  function transformDate(obj, dateKey) {
    obj.forEach(function (subObj) {
      if (subObj.hasOwnProperty(dateKey)) {
        let dateArr = subObj[dateKey].split('-');
        let year = dateArr[0];
        let month = dateArr[1];

        subObj[dateKey] = `${dateMap[month]} ${year}`;
      }
    });
  }

  // TODO: see if there's a better way to do this
  ['work', 'education'].forEach(function (section) {
    if (json.hasOwnProperty(section) && json[section].length) {
      ['startDate', 'endDate'].forEach(function (dateField) {
        transformDate(json[section], dateField);
      });
    }
  });

  const css = fs.readFileSync(__dirname + '/style.css', 'utf-8');
  const tpl = fs.readFileSync(__dirname + '/resume.hbs', 'utf-8');

  return Handlebars.compile(tpl)({
    css: css,
    resume: json
  });
}

/**
 * Handlebars helpers registration
 */

Handlebars.registerHelper('phoneLink', handlebarsHelpers.phoneLink);
Handlebars.registerHelper('textToHtml', handlebarsHelpers.textToHtml);
Handlebars.registerHelper('cityState', handlebarsHelpers.cityState);

module.exports = {
  render: render
};
