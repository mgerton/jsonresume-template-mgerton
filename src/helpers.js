'use strict';

const helpers = {
  // Generates a linkable phone number string out of a display string
  phoneLink: function phoneLink(phone) {
    return `+1${phone.replace(/\D/g, '')}`;
  },

  // Takes text string and converts for use in HTML
  textToHtml: function textToHtml(text) {
    return text.toLowerCase().replace(/[,';:]+/g, '').replace(/\s/g, '_');
  },

  // Creates a city state string (ex. Chicago, IL)
  cityState: function cityState(location) {
    let stateAbbr = location.postalCode.split(' ')[0];
    return `${location.city}, ${stateAbbr}`;
  }
};

module.exports = helpers;
