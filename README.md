# JSON Resume Theme

This is my personal theme for [JSON Resume](http://jsonresume.org/). It currently uses Handlebars for the templating and plain CSS.

## Developing Locally

Working on this not only requires NodeJS & NPM installed but also the JSON Resume CLI as well. Install it with:

```
$ npm install resume-cli
```

After installing, run `npm install` to install the dev dependencies.

### Linking to Resume Repository

Set up a symlink to the `resume` project:

```
$ ln -s ../resume/resume.json
```

This will show non-boilerplate content when the theme is served locally. This is a work-in-progress, as there may be a better way to grab the resume project (git submodule, etc).

### Serving the Theme

Run `resume serve` to bring up a local dev server at `localhost:4000`.

## Deploying

The theme currently uses a workaround to deploy from a non-registry based theme to my personal site. This command is executed by a [Gulp](http://gulpjs.com) task which builds the template then remotes into my webhost to sync the files.

Available tasks:

* `gulp clean:dist`: cleans out the built files
* `gulp build`: runs the manual export
* `gulp deploy`: builds the resume and then remotes into my web server to sync the changes
* `gulp`: default task. Same as `gulp deploy`.

## License

Available under [the MIT license](http://mths.be/mit) until further notice (or until I change my mind).
